"""
montgomery.py
----------------------------------
@author: Alexandre Poirrier
Feb 2019

This file provides functions for Montgomery ladder.
"""

"""
cswap is a conditionnal swap in constant time (does not depend of the paramaters).
Outputs: x2,x3 if swap == 0, x3,x2 otherwise.
"""
def cswap(swap, x2, x3):
    mask = 0 - swap
    dummy = mask & (x2 ^ x3)
    x2 = x2 ^ dummy
    x3 = x3 ^ dummy
    return x2, x3

"""
bezout(a,b) returns (u,v,g) such that au+bv = g where g = gcm(a,b)
"""
def bezout(a,b):
    if b == 0:
        return (1,0,1)
    u,v,g = bezout(b, a%b)
    return v, u-v*(a//b), g

"""
mod_inv(a,N) returns the inverse of a mod N if it exists, -1 otherwise
"""
def mod_inv(a,N):
    u,_,g = bezout(a,N)
    if g == 1:
        return u%N
    return -1

"""
calculate_a24 outputs the (A-2)/4 mod N parameter for the calculations
"""
def calculate_a24(A, N):
    y = mod_inv(4,N)
    if y < 0:
        print("Error : 4 not inversible modulo N")
        exit()
    return ((A-2)*y)%N

"""
ladder(N,A,m,v) outputs the coordinates x2,z2 of [m]v in the curve defined by A and N
"""
def ladder(N,A,m,v):
    k = m
    u = v[0]%N
    a24 = calculate_a24(A,N)
    
    x1 = u
    x2 = 1
    z2 = 0
    x3 = u
    z3 = v[1]%N
    swap = 0
    
    for t in range(254,-1,-1):
        kt = (k >> t) & 1
        swap ^= kt
        x2, x3 = cswap(swap, x2,x3)
        z2, z3 = cswap(swap, z2,z3)
        swap = kt
                
        A = (x2 + z2)%N
        AA = pow(A, 2, N)
        B = (x2 - z2)%N
        BB = pow(B, 2, N)
        E = (AA - BB)%N
        C = (x3 + z3)%N
        D = (x3 - z3)%N
        DA = (D*A)%N
        CB = (C*B)%N
                
        x3 = pow( DA + CB, 2, N)
        z3 = pow(DA - CB, 2, N)
        z3 = (z3*x1)%N
        x2 = (AA* BB)%N
        z2 = (E*( AA + a24 * E))%N
        
       
    
    x2,x3 = cswap(swap, x2, x3)
    z2, z3 = cswap(swap, z2, z3)
    return x2, z2
    
    
