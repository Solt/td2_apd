"""
tests.py
----------------------------------
@author: Alexandre Poirrier
Feb 2019

This file launches some tests for the Montgomery ladder and the x25519 function.
"""

import sys
from montgomery import ladder
from x25519 import X25519, encode_u, toogle_endian

if __name__ == "__main__":
    if len(sys.argv) <=1:
        print("Not enough arguments. Please read README.md")
        exit()
    if sys.argv[1] == "x25519":
        k = 0x0900000000000000000000000000000000000000000000000000000000000000
        u = 0x0900000000000000000000000000000000000000000000000000000000000000
        for i in range(int(sys.argv[2])):
            old_k = k
            k = X25519(k, u)
            u = old_k
        print(hex(k))
    elif sys.argv[1] == "curve1":
        x = 2
        z = 1
        m = int(sys.argv[2])
        p = 101
        A = 49
        u2, v2 = ladder(p, A, m, [x,z])
        print((u2*pow(v2,p-2,p))%p, 1)
    elif sys.argv[1] == "curve2":
        x = 7
        z = 1
        m = int(sys.argv[2])
        p = 1009
        A = 682
        u2, v2 = ladder(p, A, m, [x,z])
        print((u2*pow(v2,p-2,p))%p, 1)
    elif sys.argv[1] == "ECDH":
        alice_private = 0x77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a
        bob_private = 0x5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb
        neuf = 0x0900000000000000000000000000000000000000000000000000000000000000
        alice_public = X25519(alice_private, neuf)
        print("Alice's public key: " + hex(alice_public))
        bob_public = X25519(bob_private, neuf)
        print("Bob's public key: " + hex(bob_public))
        shared_secret_bob = X25519(bob_private, alice_public)
        print("Shared secret computed by Bob: " + hex(shared_secret_bob))
        shared_secret_alice = X25519(alice_private, bob_public)
        print("Shared secret computed by Alice: " + hex(shared_secret_alice))
