"""
x25519.py
----------------------------------
@author: Alexandre Poirrier
Feb 2019

This file computes the x25519 function, using the Montgomery ladder on curve25519.
"""

from montgomery import ladder
import x25519_constants as cst

#Having an hexadecimal number x, toogle_endian(x) represents x in the other endianness
def toogle_endian(x):
    s = 0
    for i in range(31,-1,-1):
        s += (x & 255) << (i*8)
        x //= 256
    return s

#decode_scalar(s) applies the algorithm used to decode scalars in the x25519 algorithm
def decode_scalar(x):
    x = toogle_endian(x)
    s = x & 248
    x -= (x & 0xff)
    x += s
    
    s = x & (0xff << 8*31)
    x -= s
    s &= (127 << 8*31)
    s |= (64 << 8*31)
    x|=s
    
    return x

#decode_u(u) applies the algorithm used to decode vectors in the x25519 algorithm
def decode_u(u):
    u = toogle_endian(u)
    u &= ((1 << 31*8+7) -1)
    return u

#encode_u prints u in the correct representation
def encode_u(u):
    return toogle_endian(u)

def X25519(k,u):
    k = decode_scalar(k)
    u = decode_u(u)
    x2,z2 = ladder(cst.p, cst.A, k, [u,1])
    return encode_u((x2*pow(z2, cst.p-2, cst.p))%cst.p)
