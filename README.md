# Montgomery arithmetic

This is an assignment for the INF568 course. The following package includes:

- files implementing Montgomery-curve operations and testing files

- a file implementing the X25519 key exchange

- a file implementing Lenstra's ECM integer factorization

- numbers factorized using this method (and the used parameters).

## Requirements

A version of Python 3 is needed. The code has been tested on Python 3.6.3 on an Ubuntu 18.04 environment.

## Usage
One can run tests on the Montgomery-curve operations. There are three curves:

### First curve
First is the curve E: Y^2*Z = X*(X^2 + A*X*Z + Z^2) with prime p = 101 and A = 49. You can compute [m]P with the command

```
python3 tests.py curve1 m
```
where P = (2:2:1).

Results are given in the form x 1 with (x:*:1) the coordinates of [m]P.

### Second curve
Second is the same curve with prime p = 1009 and A = 682. You can compute [m]P with the command

```
python3 tests.py curve2 m
```
where P = (7:207:1).

Results are given in the form x 1 with (x:*:1) the coordinates of [m]P.

### X25519
Final curve is the X25519 curve. You can compute the output after m iterations (as described in RFC 7748, with k and u = 9) with the command:

```
python3 tests.py x25519 m
```

### Elliptic Curve Diffie-Hellman
To run the example of the ECDH given in RFC, you can launch

```
python3 tests.py ECDH
```