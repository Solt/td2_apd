"""
x25519_constants.py
----------------------------------
@author: Alexandre Poirrier
Feb 2019

This file provides the constants of the curve25519 elliptic curve
"""

p = (1 << 255) - 19
A = 486662
